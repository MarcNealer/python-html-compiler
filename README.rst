------------------------
The Python HTML Compiler
------------------------

Requirements
------------

    Python 3.4

Introduction
------------

This project allows you to write python code that can be compiled to produce HTML.
This is not a templating language. We do no scan and replace elements within a template
using tags etc.
This will give you the ability to write in pure python and have that code compiled into
html files

.. contents::

Reasoning
=========
While making changes to an Angular UI, I noticed how much of my UI code was repeated
with very minor changes. For example in Angular, the same form can be used multiple
times with different data models.

Angular can render html, much the same as Jinja, Cheetah, django etc, but the issue
here is that you would need to render 2-3 times to get snippets to work like objects.

HTML does not really know what DRY means.

At the same time, I was tasked with moving a site to using Typescipt, a language that compiles
into Javascript. A language that compiles into another language

Thus I thought, what if I can get Python to create HTML. This will let me use the programming
power of python to control and define my pages.

Thus the HTML compiler

Coding Your HTML
----------------
First off, remember that this is a compiler. You will need to code python and that code
is changed into HTML. This is not a rendering or template engine. You need to know Python.

Secondly, this is a bare bones system. HTML is constantly changing and when you start adding
in JS libraries the whole things becomes a standards nightmare. For example in Angular 1, you use
directives to create your on tags or attribute, however Angular 2 creates new tags for each component you code.

Thus I have not created a system with classes for Divs, tables, etc, but the raw classes that will allow you to do so

Thus for example, if you have a site where you use the same style of table in 10 places, you can create a base
table class that will work for all of them.

This will also allow you to state defaults for items in your site, such as css classes on buttons, form layouts etc
without having to add the class each time you build a form.

You should also note that this is changing HTML into OO code. It will be slow to start with while you
create all your base classes, but as you go on it will get faster and faster


Attribute Class
===============

AttributeBase is used to create classes for the attributes you ar going to use. Since we
are coding in python we can create the class with defaults and then override if needed when we create objects

This is a basic Attribute Class

.. code :: python

    class myAttribute(AttibuteBase)
        directive = 'attribute-name'

There are three values that can be set for a given attribute.
    value : This is the value that is assigned to the attribute
    use_value : This states is a value is required or not such as an attribute like required
    value_key : This is a key value to passed parameters and if set will get the value from there not the 'value' arg

Defaults for these may be set as such

.. code :: python

    class MyAttribute(AttributeBase)
        directive = 'attribute-name'
        default_value = None
        default_use_value = False
        default_value_key = 'a_key'


Upon initalising the class you can override these values

.. code :: python

    test = MyAttribute(value='true')

All parmameter values are passed up from the tag class and its parents, so the value_key can refer to
a key:value pair set earlier in the code.

It should be noted that of no value_key is given and no value, the attribute will render a blank string

Tag Class
=========

This is your primary class for adding html tags to your page. The thing to remember is that an HTML page
is just a set of tags inside tags, just like everything in Python is an object.

To Create your own tag class, you need only set the tag name

.. code :: python

    class MyTag(TagBase):
        tag_name = 'div'

This will just render a pair of div tags

Now for the more interesting stuff.

default_parms
^^^^^^^^^^^^^

For attributes and values in your tags, you can set a series of default values here, for example
css classes, ng-click values, form names etc

.. code :: python

    class MdInputTag(TagBase):
        tag_name = 'md-input-container'
        default_parms = {'css_classes': 'md-block'}

This snippet is set to use an attribute class where the default is to get the value from 'css_classes'


Attributes
^^^^^^^^^^

Tags need attributes. Set a list of all the possible attributes using the attribute method. You return
a tuple of Attribute classes

.. code :: python

    class MdInputTag(TagBase):
        tag_name = 'md-input-container'
        default_parms = {'css_classes': 'md-block'}

        def attributes(self):
            return (CssClassAttribute(),)

So this builds an Angular Materials input container tag, with a class attribute set to a default of 'md-block'


If you have some complex calcuations to do on the attributes, you can overide the 'build-attributes()' method

.. code :: python

    class MyTag(TagBase)
        tag_name = 'div'
        def build_attributes():
            return 'class="warn blue big"'

self.newparms will give you access to a dictionary of all the passed parameters

Children
^^^^^^^^

Tags contain tags. I call them children. Like a family tree, they can have children as well, or none.
You add children to your class by overriding the children() method. You return a tuple of Tag classes

.. code :: python

    class MyTag(TagBase):
        tag_name = 'div'
        def children(self)
         return (LabelClass(),
                 TextFormClass())

Default parms
^^^^^^^^^^^^^

defaults can be set using the default_parms variable

.. code :: python

    default_parms = {'name' :'test'}

Any parms here can be overridden unpon initialisation or new ones added

.. code :: python

    test =MyClass(name='not-test',newparm='this')

Example
=======

The following renders a basic form with 3 inputs and a button

.. code :: python

    class MyForm(FormTag):
        default_parms = {'form_name':"test_form"}
        def children(self):
            return (
                MdInputTag(label='input1', type='text',
                           placeholder='input text1',
                           ng_model='test1'),
                MdInputTag(label='input2', type='text',
                           placeholder='input text2',
                           ng_model='test2'),
                MdInputTag(label='input3', type='text',
                           placeholder='input text3',
                           ng_model='test3'),
                MdButton(value='submit', ng_click='mysubmit()')
                            )

This of course uses a class FormTag as its parent
.. code :: python

    class FormTag(TagBase):
        tag_name = "form"

        def attributes(self):
            return (FormNameAttribute(),)

And of Course this uses an Attribute Class

.. code :: python

    class FormNameAttribute(AttributeBase):
        directive = 'name'
        default_value_key = 'form_name'

The inputs use this class

.. code :: python

    class MdInputTag(TagBase):
        tag_name = 'md-input-container'
        default_parms = {'css_classes': 'md-block'}

        def attributes(self):
            return (CssClassAttribute(),)

        def children(self):
            return (
                LabelTag(css_classes=''),
                InputTag(css_classes='')
                )

You will notice that this class has two children one for adding a label and the other for the text input.

.. code :: python

    class LabelTag(TagBase):
        tag_name = 'label'

.. code :: python

    class InputTag(TagBase):

        tag_name = 'input'

        def attributes(self):
            return (
                InputTypeAttribute(),
                PlaceHolderAttribute(),
                NgModelAttribute(),
                CssClassAttribute()
            )

I won't go into this any more, but you can see the idea

Compiling Your HTML
-------------------

I built the Compile commands to work in a similar fashion as running tests.

First off you need to create your HTML_Compile files. In these you create subsclasses
of the CompilerBase class. Setting the output_directory parameter is required.

In this class you create a series of methods with names starting
with 'compile'.

In each of these methods create a PageObject object. To this you pass your main Tag class, and an optional
file name as file_name parm. If no file_name is given it creates a HTML file using the same name as the tag
class. Below is an example. This will create he 'hellow world' html, with a filename of MyHtmlPage.html

.. code :: python

    from compile.compiler import *
    from tests.main import *

    class testCompile(CompilerBase):
        output_directory='.'

        def compile_test(self):
            page=HTMLPageObject(self,output_class=MyHtmlPage())
            page.write_html()

You can create multiple files like this or have multiple classes in the file

To compile these use the compile_html command as such

.. code ::

    python compile_html.py

Optional parameters here are --files where you can pass multiple filename

.. code ::

    python compile_html.py --files 'test.file1.py' 'test.file2.py'

The second is to pass a base directory. The command will then scan for all HTML_Compile* files
in that directory and sub-directories

.. code ::

    python compile_html.py --dir './tests'

the --dir option will cause the --file option to be ignored

CHANGE LOG
----------

0.2 : Added in the commands and classes needed to compile multiple classes from the command line