from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='python html compiler',
    version='0.2.0',
    description='Base library for creating Python code that will compile into html',
    url='https://bitbucket.org/MarcNealer/python-html-compiler',
    author='Marc Nealer',
    author_email='marc@willowtreesofteware.com',
    license='Apache License 2.0',
    classifiers=[
        'Environment :: Web Environment',
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    keywords='html compiler',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

)
