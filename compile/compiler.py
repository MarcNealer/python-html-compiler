from abc import ABCMeta, abstractmethod, abstractproperty


class CompilerBase(metaclass=ABCMeta):
    def __init__(self):
        pass

    @abstractproperty
    def output_directory(self):
        return ''

    def compile(self):
        for items in self.__dir__():
            if 'compile_' in items:
                print(items)
                item = getattr(self, items)
                item()


class HTMLPageObject():

    def __init__(self,parent,output_class, file_name=None):
        self.parent=parent
        self.output = output_class.compile()
        if file_name:
            self.file_name=file_name
        else:
            self.file_name=output_class.__class__.__name__

    def write_html(self):
        try:
            f = open('%s/%s.html' % (self.parent.output_directory, self.file_name), 'w')
            f.write(self.output)
            f.close()
        except:
            print('write failed')






