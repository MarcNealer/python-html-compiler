"""
  Copyright 2016 Marc Stephen Nealer

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from abc import ABCMeta, abstractmethod, abstractproperty


class AttributeBase(metaclass=ABCMeta):

    @abstractproperty
    def directive(self):
        return ''

    default_value = None
    default_value_key =''
    default_use_value = True

    def __init__(self, **kwargs):

        self.use_value = kwargs.get('use_value',self.default_use_value)
        self.value_key = kwargs.get('value_key', self.default_value_key)
        self.value = kwargs.get('value', self.default_value)

        if 'value' in kwargs:
            self.value_key = False

    def compile(self,data=None):
        if self.use_value and (not self.value and not self.value_key):
            return ""
        if self.use_value and self.value_key:
            value=''
            if self.value_key:
                if data[self.value_key]:
                    value = data[self.value_key]
            else:
                value =self.value
            if value:
                return '%s="%s"' % (self.directive, value)
            else:
                return ''
        else:
            return self.directive

