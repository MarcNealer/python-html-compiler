import importlib
import argparse
import os, sys
import inspect
from compile.compiler import CompilerBase

class CompileCommand():
    def __init__(self,args):
        print(sys.path)
        if args.file:
            self.process_files(args.file)
        else:
            self.process_dir(args.dir)

    def process_dir(self,directory_list):
        file_list=[]
        for root, dir, files in os.walk(directory_list):
            for file in files:
                if 'HTML_Compile' in file:
                    file_list.append(('%s/%s' % (root,file)).replace('/','.')[2:-2])
        print(file_list)
        if file_list:
            self.process_files(file_list)

    def process_files(self,file_list):
        for str_module in file_list:
            module = importlib.import_module(str_module)
            for items in module.__dir__():
                module_class = getattr(module, items)
                if inspect.isclass(module_class):
                    if issubclass(module_class, CompilerBase) and not inspect.isabstract(module_class):
                        runclass=module_class()
                        runclass.compile()









parser = argparse.ArgumentParser(description='Commandline Arguments to compile HTML')
parser.add_argument('--file', nargs='*')
parser.add_argument('--dir', default='.')

runcommand=CompileCommand(parser.parse_args())