"""
  Copyright 2016 Marc Stephen Nealer

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from abc import ABCMeta, abstractmethod, abstractproperty
import copy


class TagObject(metaclass=ABCMeta):

    def __init__(self, **kwargs):
        self.parms = copy.deepcopy(self.default_parms)
        self.parms.update(kwargs)
        if 'value' in self.parms:
            self.value = self.parms['value']
            del self.parms['value']
        else:
            self.value=''
        if 'value_key' in self.parms:
            self.value_key = self.parms['value_key']
            del self.parms['value_key']
        else:
            self.value_key=''

        self.tag_depth = 0

    @abstractmethod
    def children(self):
        children = ()
        return children

    @abstractmethod
    def attributes(self):
        attributes = ()
        return attributes

    @abstractproperty
    def tag_name(self):
        return ""

    @abstractproperty
    def default_parms(self):
        return {}

    def open_tag(self, tag_string):
        return "%s<%s %s>\n" % (tag_string,self.tag_name,self.build_attributes())

    def build_main(self, tag_string):
        if self.value_key:
            return "%s\t%s\n" % (tag_string,self.newparms[self.value_key])
        else:
            return "%s\t%s\n" % (tag_string,self.value)

    def close_tag(self, tag_string):
        return "%s</%s>\n" % (tag_string,self.tag_name)

    def build_attributes(self):
        """
        This Method creates the text for the attributes.

        This can be overridden, but requires the output to be a
        text string of attributes only

        example

        required class="warning"
        """
        attr=list()
        for att in self.attributes():
            attr.append(att.compile(self.newparms))
        return " ".join(attr)


    def compile(self, parms=None, tag_depth=None):

        if parms:
            self.newparms=parms
        else:
            self.newparms={}
        self.newparms.update(self.parms)

        if tag_depth:
            self.tag_depth=tag_depth

        tag_string= ''.join(['\t' for x in range(0,self.tag_depth,1)])

        html = list()
        html.append(self.open_tag(tag_string))
        for child in self.children():
            html.append(child.compile(parms=self.newparms,tag_depth=self.tag_depth+1))
        html.append(self.build_main(tag_string))
        html.append(self.close_tag(tag_string))
        return ''.join(html)

class TagBase(TagObject):
    tag_name=''
    default_parms = {}
    def children(self):
        return ()
    def attributes(self):
        return ()


