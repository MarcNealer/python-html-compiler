from compile.Attributes import AttributeBase


class NgModelAttribute(AttributeBase):
        directive = 'ng-model'
        default_value_key = 'ng_model'

class NgClickAttribute(AttributeBase):
    directive = 'ng-click'
    default_value_key = 'ng_click'

class FormNameAttribute(AttributeBase):
    directive = 'name'
    default_value_key = 'form_name'

class CssClassAttribute(AttributeBase):
    directive = 'class'
    default_value_key = 'css_classes'

class InputTypeAttribute(AttributeBase):
    directive = 'type'
    default_value_key = 'type'


class PlaceHolderAttribute(AttributeBase):
    directive = 'placeholder'
    default_value_key = 'placeholder'




