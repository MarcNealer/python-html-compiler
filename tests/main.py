from tests.tags import *
from compile.compiler import *

class MyBody(Body):
    def children(self):
        return (
        Div(value="Hello World"),
    )

class MyHeader(Header):
    def children(self):
        return ()

class MyHtmlPage(Html):
    __data__={}
    def children(self):
        return (MyHeader(),
                MyBody())


