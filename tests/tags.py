from compile.Tags import TagBase
from tests.attributes import *


class MdButton(TagBase):
    tag_name = "md-button"

    def attributes(self):
        return (NgClickAttribute(),
                )


class MdContents(TagBase):
    tag_name = "md-contents"



class Div(TagBase):
    tag_name = 'div'



class Html(TagBase):
    tag_name = 'html'



class Header(TagBase):
    tag_name = 'header'



class Body(TagBase):
    tag_name = 'body'


class LabelTag(TagBase):
    tag_name = 'label'


class FormTag(TagBase):
    tag_name = "form"

    def attributes(self):
        return (FormNameAttribute(),)

class InputTag(TagBase):

    tag_name = 'input'

    def attributes(self):
        return (
            InputTypeAttribute(),
            PlaceHolderAttribute(),
            NgModelAttribute(),
            CssClassAttribute()
        )


class MdInputTag(TagBase):
    tag_name = 'md-input-container'
    default_parms = {'css_classes': 'md-block'}

    def attributes(self):
        return (CssClassAttribute(),)

    def children(self):
        return (
            LabelTag(css_classes=''),
            InputTag(css_classes='')
            )
