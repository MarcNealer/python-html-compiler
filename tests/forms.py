from tests.tags import *
from tests.attributes import *

class MyForm(FormTag):
    default_parms = {'form_name':"test_form"}
    def children(self):
        return (
            MdInputTag(label='input1', type='text',
                       placeholder='input text1',
                       ng_model='test1'),
            MdInputTag(label='input2', type='text',
                       placeholder='input text2',
                       ng_model='test2'),
            MdInputTag(label='input3', type='text',
                       placeholder='input text3',
                       ng_model='test3'),
            MdButton(value='submit', ng_click='mysubmit()')
                        )